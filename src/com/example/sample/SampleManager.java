package com.example.sample;

import java.util.ArrayList;

import android.content.Context;

public class SampleManager implements ISample {

    private Context mContext = null;
    private ArrayList<SampleCallback> mCallbacks = null;

    public interface SampleCallback {
        public void onInited();

        public void test();
    }

    public SampleManager(Context context) {
        mContext = context;
        mCallbacks = new ArrayList<SampleCallback>();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void init() {
        fireOnInited();
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean registCallback(SampleCallback callback) {
        if (mCallbacks == null) {
            return false;
        }
        if (mCallbacks.contains(callback)) {
            return false;
        }
        return mCallbacks.add(callback);
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean unregistCallback(SampleCallback callback) {
        if (mCallbacks == null) {
            return false;
        }
        if (!mCallbacks.contains(callback)) {
            return false;
        }
        return mCallbacks.remove(callback);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void recycle() {
        if (mCallbacks != null) {
            mCallbacks.clear();
            mCallbacks = null;
        }
        mContext = null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public ISample getSampleManager() {
        return this;
    }

    private void fireOnInited() {
        if (mCallbacks == null) {
            return;
        }
        for (SampleCallback callback : mCallbacks) {
            callback.onInited();
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void doTest() {
        if (mContext == null) {
            return;
        }
        fireOnTest();
    }

    private void fireOnTest() {
        if (mCallbacks == null) {
            return;
        }
        for (SampleCallback callback : mCallbacks) {
            callback.test();
        }
    }
}
