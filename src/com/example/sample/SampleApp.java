package com.example.sample;

import android.app.Application;

public class SampleApp extends Application {

    private static SampleManager mSampleManager = null;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        mSampleManager = new SampleManager(this);
    }

    @Override
    public void onTerminate() {
        recycle();
        super.onTerminate();
    }

    public ISample getSampleManager() {
        return mSampleManager;
    }

    public void recycle() {
        if (mSampleManager != null) {
            mSampleManager.recycle();
            mSampleManager = null;
        }
    }
}
