package com.example.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.example.sample.SampleManager.SampleCallback;

public class MainActivity extends Activity {

    private static ISample mISample = null;
    private SampleCallback mCallback = new SampleCallback() {

        @Override
        public void test() {
            showToastUiThread("sample test is finish on " + MainActivity.class.getSimpleName());
        }

        @Override
        public void onInited() {
            showToastUiThread("sample manager is inited on " + MainActivity.class.getSimpleName());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mISample != null) {
            mISample.registCallback(mCallback);
        }
    }

    @Override
    protected void onPause() {
        if (mISample != null) {
            mISample.unregistCallback(mCallback);
        }
        super.onPause();
    }

    private void init() {
        mISample = ((SampleApp) getApplication()).getSampleManager();
        findViewById(R.id.btn_init).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mISample != null) {
                    mISample.init();
                }
            }
        });
        findViewById(R.id.btn_test).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mISample != null) {
                    mISample.doTest();
                }
            }
        });
    }

    private void showToastUiThread(final String text) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                showToast(text);
            }
        });
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
