package com.example.sample;

import com.example.sample.SampleManager.SampleCallback;

public interface ISample {

    public void init();
    public boolean registCallback(SampleCallback callback);
    public boolean unregistCallback(SampleCallback callback);
    public ISample getSampleManager();
    public void doTest();
    public void recycle();
}
